﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControls : MonoBehaviour {
	public AudioSource _attackSound;
	public AudioSource _jumpSound;

	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
            if (GetComponent<Movement>().Jump())
            {
                _jumpSound.Play();
            }
		}
		if (Input.GetKeyDown (KeyCode.M)) {
            if (GetComponent<AttackLauncher>().Attack(GetComponent<Movement>().IsFacingRight, true, true))
            {
                _attackSound.Play();
            }
		}
        else if (Input.GetKeyDown(KeyCode.K))
           GetComponent<AttackLauncher>().Attack(GetComponent<Movement>().IsFacingRight,false,true);

        
        GetComponent<Movement> ().Move (Input.GetAxis ("Horizontal"));
	}
}
