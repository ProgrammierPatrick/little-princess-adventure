﻿using Assets.Scripts.LevelManager;
using UnityEngine;
using Assets.Scripts.Math;

public class EnemyAI : MonoBehaviour {

    public bool melee;
    public bool turnOnEdge;

    public int Direction { get;private set; }


    // Use this for initialization
    void Start () {
        Direction = 1;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (turnOnEdge)
        {
            var solids = GameObject.FindWithTag("Level").GetComponent<LevelManager>().SolidObjects;
            var pos = transform.position;
            int x = Mathf.RoundToInt(pos.x);
            int y = Mathf.RoundToInt(pos.y);
            TwoDPoint point = new TwoDPoint(x, y);
            while (true)
            {
                if (solids.Contains(point))
                    break;
                else point.Y--;
            }
            while (true)
            {
                if (!solids.Contains(point))
                    break;
                else point.X += Direction;
            }
            var dif = Mathf.Abs(x - point.X);
            if (dif < 2) {
                Direction *= -1;
            }
        }

        if (!melee)
            GetComponent<AttackLauncher>().Attack(GetComponent<Movement>().IsFacingRight, true, false);
        GetComponent<Movement>().Move(Direction);
        if (_colFlag) DoCol(Time.deltaTime);
    }

    private bool _colFlag = false;
    private float _time=-10;
    private float _coolTime = 0.1f;

    private void DoCol(float time) {
        _time -= time;
        if(_time<=0)
        {
            Direction *= -1;
        }
        _colFlag = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var col = collision.collider.GetComponent<Health>();
        if (melee && col != null && col._friend)
        {
            GetComponent<AttackLauncher>().Attack(GetComponent<Movement>().IsFacingRight, true, false);
        }
        else
        if (Mathf.Abs(collision.contacts[0].normal.normalized.x) > 0.6)
        {
            _colFlag = true;
            if (_time <= 0)
            {
                _time = _coolTime;
            }
            
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        OnCollisionEnter2D(collision);
    }
}
