﻿internal static class LayerNames
{
    public const string Background = "background";
    public const string Objects = "objects";
    public const string Foreground = "foreground";
}