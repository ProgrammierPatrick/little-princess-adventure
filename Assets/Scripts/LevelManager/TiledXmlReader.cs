﻿using Assets.Scripts.Math;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using UnityEngine;

namespace Assets.Scripts.LevelManager
{
    internal class TiledXmlReader
    {
        private XDocument _tmxAsDocument;
        private const int TileWidthAndHeight = 478;

        private TiledXmlReader(XDocument tmxAsDocument)
        {
            _tmxAsDocument = tmxAsDocument;
        }

        internal static TiledXmlReader CreateInstance(string tmxAsString)
        {
            return new TiledXmlReader(XDocument.Parse(tmxAsString));
        }

        internal IEnumerable<Layer> LoadLayers()
        {
            var layers = new List<Layer>();

            LoadLayerIfExists(layers, LayerNames.Background, -1, false);
            LoadLayerIfExists(layers, LayerNames.Objects, 0, true);
            LoadLayerIfExists(layers, LayerNames.Foreground, 1, false);

            return layers;
        }

        internal IEnumerable<Sprite> LoadSprites()
        {
            var regex = new Regex(@"(?:.*\/)?(.+)\.json$");

            var spriteFolders = _tmxAsDocument
                .Descendants("tileset")
                .Select(x =>
                    new SpriteFolder(
                        int.Parse(x.Attribute("firstgid").Value),
                        regex.Replace(x.Attribute("source").Value, "$1")))
                .ToArray();

            var sprites = new List<Sprite>();

            foreach (var spriteFolder in spriteFolders)
            {
                foreach (var sprite in Resources.LoadAll<Sprite>("Sprites/" + spriteFolder.SpriteFolderName))
                    sprites.Add(sprite);
            }

            return sprites;
        }

        internal SpawnPoints LoadSpawnPoints()
        {
            var dynamicElements = _tmxAsDocument
                .Descendants("objectgroup")
                .Single(x => x.Attribute("name").Value.Equals("dynamic"))
                .Descendants()
                .ToArray();

            var wrongDynamicElementNames = dynamicElements
                .Select(x => x.Attribute("name").Value)
                .Where(x => !x.StartsWith("S_"))
                .ToArray();

            if (wrongDynamicElementNames.Any())
                Debug.LogWarning("Wrong SpawnPoint names: " + string.Join(", ", wrongDynamicElementNames));

            var allSpawnPoints = dynamicElements
                .Where(x => x.Attribute("name").Value.StartsWith("S_"))
                .Select(x => new SpawnPoint(
                    x.Attribute("name").Value.Substring(2),
                    new TwoDPoint(
                        (int.Parse(x.Attribute("x").Value) + int.Parse(x.Attribute("width").Value) / 2) / TileWidthAndHeight,
                        int.Parse(x.Attribute("y").Value) / TileWidthAndHeight)))
                .ToArray();

            return new SpawnPoints(
                allSpawnPoints.Where(x => !x.EntityName.Equals("Player")).ToArray(),
                allSpawnPoints.Single(x => x.EntityName.Equals("Player")));
        }

        private void LoadLayerIfExists(IList<Layer> layers, string layerName, int orderInLayer, bool withCollision)
        {
            var layer = LoadLayer(layerName, orderInLayer, withCollision);

            if (layer == null)
                return;

            layers.Add(layer);
        }

        private Layer LoadLayer(string layerName, int orderInLayer, bool withCollision)
        {
            var layerElement = _tmxAsDocument
                .Descendants("layer")
                .SingleOrDefault(x => x.Attribute("name").Value.Equals(layerName));

            if (layerElement == null)
                return null;

            var layerCSV = layerElement
                .Descendants()
                .First()
                .Value;

            if (!LayerMetaData.IsInitialized)
            {
                var amountOfrows = int.Parse(layerElement.Attribute("height").Value);
                var amountOfcolumns = int.Parse(layerElement.Attribute("width").Value);

                LayerMetaData.Initialize(amountOfrows, amountOfcolumns);
            }

            return new Layer(layerCSV, orderInLayer, withCollision);
        }
    }
}