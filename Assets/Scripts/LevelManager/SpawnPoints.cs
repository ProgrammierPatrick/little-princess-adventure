﻿using System.Collections.Generic;

namespace Assets.Scripts.LevelManager
{
    internal class SpawnPoints
    {
        internal SpawnPoints(IEnumerable<SpawnPoint> enemiesSpawnPoints, SpawnPoint playerSpawnPoint)
        {
            EnemiesSpawnPoints = enemiesSpawnPoints;
            PlayerSpawnPoint = playerSpawnPoint;
        }

        public IEnumerable<SpawnPoint> EnemiesSpawnPoints { get; private set; }
        public SpawnPoint PlayerSpawnPoint { get; private set; }
    }
}