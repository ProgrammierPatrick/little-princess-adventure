﻿using Assets.Scripts.Math;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.LevelManager
{
    public class LevelManager : MonoBehaviour
    {
        public TextAsset _LevelFile;
        public GameObject _standardTilePrefab;
        public GameObject _collisionTilePrefab;

        public HashSet<TwoDPoint> SolidObjects { get; private set; }

        void Awake()
        {
            var tiledXmlReader = TiledXmlReader.CreateInstance(_LevelFile.text);

            var layerParser = new LayerParser(() => Instantiate(_standardTilePrefab), () => Instantiate(_collisionTilePrefab), x => Instantiate(x));
            
            SolidObjects = layerParser.CreateUnityGameObjects(tiledXmlReader.LoadLayers(), tiledXmlReader.LoadSprites(), tiledXmlReader.LoadSpawnPoints());
        }
    }
}