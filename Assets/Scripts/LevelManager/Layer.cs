﻿namespace Assets.Scripts.LevelManager
{
    internal class Layer
    {
        public Layer(string layerCSV, int orderInLayer, bool withCollision)
        {
            LayerCSV = layerCSV;
            OrderInLayer = orderInLayer;
            WithCollision = withCollision;
        }

        internal string LayerCSV { get; private set; }
        internal int OrderInLayer { get; private set; }
        internal bool WithCollision { get; private set; }
    }
}
