﻿namespace Assets.Scripts.LevelManager
{
    internal static class LayerMetaData
    {
        internal static bool IsInitialized { get; private set; }

        internal static int AmountOfRows { get; private set; }
        internal static int AmountOfColumns { get; private set; }

        internal static void Initialize(int amountOfRows, int amountOfColumns)
        {
            IsInitialized = true;
            AmountOfRows = amountOfRows;
            AmountOfColumns = amountOfColumns;
        }
    }
}
