﻿using Assets.Scripts.Math;

namespace Assets.Scripts.LevelManager
{
    internal class SpawnPoint
    {
        internal SpawnPoint(string entityName, TwoDPoint spawnPointCoordinates)
        {
            EntityName = entityName;
            SpawnPointCoordinates = spawnPointCoordinates;
        }

        public string EntityName { get; private set; }
        public TwoDPoint SpawnPointCoordinates { get; private set; }
    }
}