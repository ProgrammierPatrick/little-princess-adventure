﻿namespace Assets.Scripts.LevelManager
{
    internal class SpriteFolder
    {
        public SpriteFolder(int firstTileId, string spriteFolderName)
        {
            FirstTileId = firstTileId;
            SpriteFolderName = spriteFolderName;
        }

        internal int FirstTileId { get; private set; }
        internal string SpriteFolderName { get; private set; }
    }
}
