﻿using Assets.Scripts.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Assets.Scripts.LevelManager
{
    internal class LayerParser
    {
        private readonly Func<GameObject> _createStandardTile;
        private readonly Func<GameObject> _createCollisionTile;
        private readonly Func<GameObject, GameObject> _createGameObject;
        private readonly HashSet<TwoDPoint> _solidObjects;

        private Sprite[] _sprites;

        public LayerParser(Func<GameObject> createStandardTile, Func<GameObject> createCollisionTile, Func<GameObject, GameObject> createGameObject)
        {
            _createStandardTile = createStandardTile;
            _createCollisionTile = createCollisionTile;
            _createGameObject = createGameObject;
            _solidObjects = new HashSet<TwoDPoint>();
        }

        internal HashSet<TwoDPoint> CreateUnityGameObjects(IEnumerable<Layer> layers, IEnumerable<Sprite> sprites, SpawnPoints spawnPoints)
        {
            _sprites = sprites.ToArray();

            SpawnGameObjects(spawnPoints);

            foreach (var layer in layers)
                CreateLayerFromCSV(layer);

            return _solidObjects;
        }

        private void CreateLayerFromCSV(Layer layer)
        {
            var y = LayerMetaData.AmountOfRows-1;

            foreach (var row in layer.LayerCSV.Split('\n'))
            {
                var x = 0;

                if (Regex.IsMatch(row, "^\\s+$"))
                    continue;

                foreach (var column in row.Split(','))
                {
                    uint tileId = 0;

                    if (!uint.TryParse(column, out tileId))
                        continue;

                    if (tileId != 0)
                    {
                        var tileCoordinates = new TwoDPoint(x, y);

                        InitializeTile(layer, tileCoordinates, tileId);
                        AddToSolidObjectsIfNecessary(layer, tileCoordinates);
                    }

                    x++;
                }

                y--;
            }
        }

        private void AddToSolidObjectsIfNecessary(Layer layer, TwoDPoint tileCoordinates)
        {
            if (!layer.WithCollision)
                return;

            _solidObjects.Add(tileCoordinates);
        }

        private void InitializeTile(Layer layer, TwoDPoint tileCoordinates, uint tileId)
		{
			const uint FLIPPED_HORIZONTALLY_FLAG = 0x80000000;
			const uint FLIPPED_VERTICALLY_FLAG = 0x40000000;
			const uint FLIPPED_DIAGONALLY_FLAG = 0x20000000;

			bool flip_h = (tileId & FLIPPED_HORIZONTALLY_FLAG) > 0;
			bool flip_v = (tileId & FLIPPED_VERTICALLY_FLAG) > 0;
			bool flip_d = (tileId & FLIPPED_DIAGONALLY_FLAG) > 0;

			tileId &= ~(FLIPPED_HORIZONTALLY_FLAG |
				FLIPPED_VERTICALLY_FLAG |
				FLIPPED_DIAGONALLY_FLAG);

            var tile = layer.WithCollision ? _createCollisionTile() : _createStandardTile();

            tile.name = "Tile-" + tileCoordinates.X + "-" + tileCoordinates.Y;

            var transform = tile.GetComponent<Transform>();

            transform.position = new Vector3(tileCoordinates.X, tileCoordinates.Y, 0);

            var renderer = tile.GetComponent<SpriteRenderer>();

            renderer.sortingOrder = layer.OrderInLayer;
            
            renderer.sprite = _sprites[tileId - 1];

			renderer.flipX = flip_h;
			renderer.flipY = flip_v;
        }

        private void SpawnGameObjects(SpawnPoints spawnPoints)
        {
            CreateSpawnPointGameObject(spawnPoints.PlayerSpawnPoint);

            foreach (var spawnPoint in spawnPoints.EnemiesSpawnPoints)
            {
                CreateSpawnPointGameObject(spawnPoint);
            }
        }

        private void CreateSpawnPointGameObject(SpawnPoint spawnPoint)
        {
            var spawnedGameObject = _createGameObject(Resources.Load<GameObject>("Prefabs/" + spawnPoint.EntityName));

            spawnedGameObject.transform.position = new Vector3(spawnPoint.SpawnPointCoordinates.X, LayerMetaData.AmountOfRows - 1 - spawnPoint.SpawnPointCoordinates.Y, 0);
            spawnedGameObject.name = spawnPoint.EntityName;
        }
    }
}