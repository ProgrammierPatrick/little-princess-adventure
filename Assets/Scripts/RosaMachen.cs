﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public class RosaMachen : MonoBehaviour {
	private bool isRosa = false;

	void Update () {
		if (GameObject.Find ("Player") == null)
			return;
		
		if (!isRosa && GameObject.Find ("Player").transform.position.x >= transform.position.x) {
			var regex = new Regex(@"(.+)_(\d+)");

			var renderer = GetComponent<SpriteRenderer> ();

			var spriteSheetName = "Sprites/" + regex.Replace (renderer.sprite.name, "$1") + "_rosa";
			var array = Resources.LoadAll<Sprite>(spriteSheetName);
			if(array.Length == 0)
				return;
			int index = int.Parse (regex.Replace (renderer.sprite.name, "$2"));

			try {
				renderer.sprite = array [index];
			} catch(System.IndexOutOfRangeException e) {
				Debug.LogError ("Index out of Range when turning Tile '" + renderer.sprite.name + "' pink: index " + index);
			}

			isRosa = true;
		}
	}
}
