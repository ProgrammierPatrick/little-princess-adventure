﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

[Serializable]
public class DialogLine {
	public bool isPerson1Saying = true;

	[Multiline]
	public string text = "";
}

public class Dialog : MonoBehaviour {
	public Text person1Text;
	public Text person2Text;

	public DialogLine[] dialogData;

	private int dialogIndex = 0;

	// Use this for initialization
	void Start () {
		person1Text.gameObject.SetActive (false);
		person2Text.gameObject.SetActive (false);
		NextLine ();
	}

	public void NextLine() {
		if (dialogIndex >= dialogData.Length) {
			SceneManager.LoadScene("level1");
			return;
		}

		person1Text.gameObject.SetActive (false);
		person2Text.gameObject.SetActive (false);
		if (dialogData [dialogIndex].isPerson1Saying) {
			person1Text.gameObject.SetActive (true);
			person1Text.text = dialogData [dialogIndex].text;
		} else {
			person2Text.gameObject.SetActive (true); 
			person2Text.text = dialogData [dialogIndex].text;
		}
		dialogIndex++;
	}
}
