﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimater : MonoBehaviour {
	private const int WALK_LIMIT = 8;
    private const int ATTACK_FRAMENUM = 20;

	public Movement _movementComponent;

	//frame 0: standing
	public GameObject[] _walkFrames;
	//frame 0: not attacking
	public GameObject[] _attackFrames;

	private int walkFrameCount = 0;
    private int attackFrameCount = 0;

	public int WalkIndex { get; private set; }
	public int AttackIndex { get; private set; }

	void Awake() {
		WalkIndex = AttackIndex = 0;
	}

	void Start() {
		_walkFrames [0].SetActive (true);
		for (int i = 1; i < _walkFrames.Length; ++i) {
			_walkFrames [i].SetActive (false);
		}
		_attackFrames [0].SetActive (true);
		for (int i = 1; i < _attackFrames.Length; ++i) {
			_attackFrames [i].SetActive (false);
		}
	}

    // start visual attack sequence
    public void Attack()
    {
        attackFrameCount = ATTACK_FRAMENUM;

        _attackFrames[0].SetActive(false);
        _attackFrames[1].SetActive(true);
    }

	void Update () {

        //------ WALKING --------
		walkFrameCount++;

		if (_movementComponent.IsFacingRight)
			transform.localScale = new Vector3 (1, 1, 1);
		else
			transform.localScale = new Vector3 (-1, 1, 1);
		
		//walk cycle
		if ( walkFrameCount >= WALK_LIMIT) {
			_walkFrames [WalkIndex].SetActive (false);

			if(_movementComponent.IsWalking)
				WalkIndex++;
			else WalkIndex = 0;

			if (WalkIndex >= _walkFrames.Length)
				WalkIndex = 0;

			_walkFrames [WalkIndex].SetActive (true);

			walkFrameCount = 0;
		}

        //-------- ATTACKING ---------
        if(attackFrameCount > 0)
        {
            attackFrameCount--;
            if(attackFrameCount <= 0)
            {
                _attackFrames[0].SetActive(true);
                _attackFrames[1].SetActive(false);
            }
        }
	}
}
