﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackLauncher : MonoBehaviour
{


    public GameObject _Attack1Prefab;
    public float _Attack1Speed;
    public Vector3 _attack1Spawn;
    public GameObject _Attack2Prefab;
    public float _Attack2Speed;
    public Vector3 _attack2Spawn;

    public float CoolDown { get; set; }

    void Update()
    {
        CoolDown -= Time.deltaTime;
    }
    
    public bool Attack(bool direction, bool prim, bool friend)
    {
        if (CoolDown >= 0)
        {
            return false;
        }

        GetComponent<CharacterAnimater>().Attack();

        CoolDown = prim ? _Attack1Speed : _Attack2Speed;
        Vector3 rPos = prim ? _attack1Spawn : _attack2Spawn;
        rPos.x *= direction ? 1 : -1;
        var inst = Instantiate(prim ? _Attack1Prefab : _Attack2Prefab, transform.position + rPos, Quaternion.identity);

        float x = inst.transform.localScale.x;
        x = direction ? x : -x;
        float y = inst.transform.localScale.y;
        float z = inst.transform.localScale.z;
        inst.transform.localScale = new Vector3(x, y, z);

        var att = inst.GetComponent<Attack>();
        if (att != null)
        {
            att.InitVelocity(direction);
            att._friend = friend;
        }
        else
        {
            Debug.LogWarning(inst + " is not an Attack");
        }
        return true;

    }

}
