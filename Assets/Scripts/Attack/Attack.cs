﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {
    public float _damage = 1;
    public bool _isInstant = true;  //instant: one hit only, instead of continuus damage (1 per second)
    // public float splashRadius = 0;
    public bool _isSplash = false;
    //public bool _doExplosionOnTarget = true;

    [HideInInspector]
    public bool _friend;
    public Vector2 _initVelocity;

    public GameObject visualEffectPrefab;
    public GameObject spawnOnCollision;

    private IList<Health> currentHealths = new List<Health>();
    
    public void InitVelocity(bool direction)
    {
        Vector2 v = _initVelocity;
        v.x *= direction ? 1 : -1;
        GetComponent<Rigidbody2D>().velocity =v;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_isInstant) return;

        Health h = collision.GetComponent<Health>();
        if (h != null)
        {
            bool friend = h._friend;
            if (!_isSplash&&(friend!=_friend)) {
                h.DoDamage(_damage,_friend);
                Instantiate(visualEffectPrefab, collision.GetComponent<Collider2D>().transform.position, Quaternion.identity);
            }

            currentHealths.Add(h);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Health h = collision.GetComponent<Health>();
        if (h != null)
        {
            currentHealths.Remove(h);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (_isInstant) return;

        Health h = collision.GetComponent<Health>();
        if (h != null)
        {
            bool friend = h._friend;
            Debug.Log(friend == _friend);
            if (!_isSplash && (friend != _friend))
            {
                h.DoDamage(_damage * Time.deltaTime,_friend);
                Instantiate(visualEffectPrefab, collision.GetComponent<Collider2D>().transform.position, Quaternion.identity);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!(spawnOnCollision == null)) {
            Instantiate(spawnOnCollision, transform.position, Quaternion.identity);
            if (!_isSplash)
            {
                Destroy(gameObject);
            }
        }
        if (_isSplash)
        {
            foreach (var health in currentHealths)
            {
                health.DoDamage(_damage,_friend);
            }

            Instantiate(visualEffectPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
