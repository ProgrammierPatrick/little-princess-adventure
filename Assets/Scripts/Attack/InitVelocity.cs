﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitVelocity : MonoBehaviour {

    public Vector2 _velocity;

    private void Awake()
    {
        GetComponent<Rigidbody2D>().velocity = _velocity;
    }
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
