﻿using UnityEngine;

public class Health : MonoBehaviour {
    public float _health = 1;
    public bool _friend;
    public void Kill() {
        if (GetComponent<PlayerControls>() != null)
        {
            //TODO: show game over and respawn player
            Application.LoadLevel(Application.loadedLevel);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void DoDamage(float damage, bool friend)
    {
        if (friend != _friend)
        {
            _health -= damage;
            if (_health <= 0)
            {
                Kill();
            }
        }
    }
}
