﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifetime : MonoBehaviour {
    public float _lifetime = 1; // in seconds

	void Update () {
        _lifetime -= Time.deltaTime;
        if (!(_lifetime > 0)) Destroy(gameObject);
	}
}
