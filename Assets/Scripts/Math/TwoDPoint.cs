﻿namespace Assets.Scripts.Math
{
    public class TwoDPoint
    {
        public int X { get; set; }
        public int Y { get; set; }

        public TwoDPoint(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (obj.GetType() != typeof(TwoDPoint))
            {
                return false;
            }

            var p = (TwoDPoint) obj;

            return (X == p.X) && (Y == p.Y);
        }

        public bool Equals(TwoDPoint p)
        {
            if (p == null)
            {
                return false;
            }

            return (X == p.X) && (Y == p.Y);
        }

        public override string ToString()
        {
            return "[" + X + "|" + Y + "]";
        }


        public override int GetHashCode()
        {
            return X ^ Y;
        }
    }
}
