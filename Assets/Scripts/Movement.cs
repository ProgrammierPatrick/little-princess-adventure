﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
	public float _maxSpeed = 30;
	private float _acceleration = 100;
	private float _jumpForce = 800;
    
    public bool IsFacingRight { get; private set; }
    public bool IsWalking { get; private set; }
    public bool OnGround { get; private set; }

    public bool Jump() {

        if (OnGround)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * _jumpForce);
            return true;
        }
        return false;
	}

	//should be called every UPdate() frame
	//direction: from interval [-1,1]
	public void Move(float direction) {
        float height = transform.position.y;
        if(height < -3)
        {
            GetComponent<Health>().Kill();
        }
        if (direction > 0) IsFacingRight = true;
        else if (direction < 0) IsFacingRight = false;

        float speed = GetComponent<Rigidbody2D>().velocity.x;

        IsWalking = (Mathf.Abs(direction) > 0.01) && (Mathf.Abs(speed) > 0.01);

       // CurrentSpeed = speed;
        float force = _acceleration * direction - _acceleration / _maxSpeed * speed;

        GetComponent<Rigidbody2D>().AddForce(Vector2.right * force);
        transform.rotation = Quaternion.identity;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {

        if (Mathf.Abs(collision.contacts[0].normal.normalized.y) > 0.6)
        {
            OnGround = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
            OnGround = false;
    
    }
}
